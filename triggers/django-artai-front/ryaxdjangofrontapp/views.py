# Create your views here.
import json
import os
import re

import requests
from django.shortcuts import render

from .forms import QuoteForm


def _convertor(str):
    print("convertor input ==begin==>" + str + "<==end==")
    m = re.search(r"(?<=output)[\W\w]+", str)
    new_str = m.group(0)
    print("convertor print #1 =: " + new_str)
    m = re.search(r"([\W\w]+)\]", new_str)
    new_str = m.group(0)[4:-2]
    print("convertor print #2 =: " + new_str)
    return new_str


def index(request):
    last_image_url = ""

    # If this is a POST request then process the Form data
    if request.method == "POST":
        # Create a form instance and populate it with data from the request (binding):
        form = QuoteForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            json_data = {
                "url": os.environ["REPLICATE_URL"],
                "prompt": form.cleaned_data["prompt"],
                "token": os.environ["REPLICATE_TOKEN"],
            }
            print("POST on ====> ")
            print(
                f"curl -X POST {os.environ['RYAX_ARTAI_URL']} --json '{json.dumps(json_data)}'"
            )
            resp = requests.post(os.environ["RYAX_ARTAI_URL"], json=json_data)
            if resp.status_code == 200:
                json_return = resp.json()
                print("JSON RESP ======================> " + json.dumps(json_return))
                if "result_link" in json_return:
                    print("GET on ====> ")
                    print(f"curl {json_return['result_link']}")
                    resp2 = requests.get(json_return["result_link"])
                    print("JSON RESP2 ======================> " + resp2.text)
                    if resp.status_code == 200:
                        last_image_url = _convertor(resp2.text)
                else:
                    last_image_url = _convertor(resp.text)
            print("Image URL is ========>" + last_image_url)
    # If this is a GET (or any other method) create the default form.
    else:
        form = QuoteForm()

    domain_url = os.environ["REDIRECT_PREFIX"]
    print("Domain URL ===> " + domain_url)

    context = {
        "form": form,
        "ryax_prefix": domain_url,
        "last_image": last_image_url,
    }
    return render(request, "home.html", context)
