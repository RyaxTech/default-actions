from django.forms import ModelForm

from .models import artai_prompt


class QuoteForm(ModelForm):
    class Meta:
        model = artai_prompt
        fields = "__all__"
        exclude = []
